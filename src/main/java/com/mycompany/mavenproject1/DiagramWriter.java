/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.SpreadsheetVersion;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFChartSheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


/**
 *
 * @author BanSabai
 */
public class DiagramWriter {

    public static void main(String[] args) {
        try {
            new DiagramWriter().writeSimpleTemplate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void writeSimpleTemplate() {

        try {
            XSSFWorkbook wb = new XSSFWorkbook(OPCPackage.open("template.xlsm"));
            XSSFSheet sheet = wb.getSheetAt(0);
            
            List<XSSFTable> tables = sheet.getTables();
            
            XSSFRow row1 = sheet.createRow(3);
            row1.createCell(0).setCellValue("IT vmi Főosztály");
            row1.createCell(1).setCellValue(5.4);

            sheet.createRow(4).createCell(0).setCellValue("Mega mega Főosztály");
            sheet.getRow(4).createCell(1).setCellValue(3.33);

            sheet.createRow(5).createCell(0).setCellValue("Bullshit bingo Főosztály");
            sheet.getRow(5).createCell(1).setCellValue(8.3);

            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            
             AreaReference areaReference = new AreaReference("Munka1!A$3:B$6", SpreadsheetVersion.EXCEL2007);
            for (XSSFTable table : tables) {
                table.setCellReferences(areaReference);
            }
            
            XSSFChartSheet chartSheet = (XSSFChartSheet) wb.getSheet("Diagram1");
            chartSheet.addMergedRegion(CellRangeAddress.valueOf("Munka1!A$3:B$6"));
            
            
            FileOutputStream fileOut = new FileOutputStream("report.xlsm");
            wb.write(fileOut);
            fileOut.close();
            
        } catch (InvalidFormatException ex) {
            Logger.getLogger(DiagramWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DiagramWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DiagramWriter.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
